import qrcode
import random
import base64
import io



def get_qrcode(info_string: str):
    qr = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_H, box_size=7,border=7,)
    qr.add_data(info_string)
    qr.make(fit=True)
    img = qr.make_image()

    buffered = io.BytesIO()
    img.save(buffered)
    img_str = base64.b64encode(buffered.getvalue()).decode("utf-8")
    return img_str, info_string
