

def start_generate_html():
    return """
        <html>
            <head>
                <title>Добро пожаловать на платформу MentorEventConnect</title>
                <style>
                    body {
                        background-color: white;
                        font-family: Arial, sans-serif;
                        text-align: center;
                    }

                    h1 {
                        margin-top: 80px;
                    }
                </style>
            </head>
            <body>
                <h1>Добро пожаловать на платформу MentorEventConnect</h1>
                <p>Мы делаем людей ближе и хотим делиться с ними знаниями.</p>
                <p>Для проверки работы API платформы перейдите по ссылке <a href="http://0.0.0.0:8000/docs">здесь</a>.</p>
                <p>Автор: Сурнов Алексей.</p>
            </body>
        </html>
        """
