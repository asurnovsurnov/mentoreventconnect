from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from qrgeneratorinfo.startpagehtml import start_generate_html as base_html_page
import uvicorn
from dbcon import create_database_if_not_exists, create_shema_for_database
from dbcon.database import datadb, check_database_exists
from router import routesmentor, routesadmin, routesguest, routesbase


app = FastAPI(title='MentorEventConnect',
              description='The project is designed to create and register for events. \n'
                          'Our goal is for people who have something to say and show to be able to present their achievements to the world. \n'
                          'It is a platform that brings together mentors and guests to participate in a variety of events. \n'
                          'From informative lectures to captivating musical concerts and engaging presentations, \n'
                          'MentorEventConnect provides guests with the opportunity to discover events that interest them and easily register using a simple and user-friendly interface.')



@app.get("/", response_class=HTMLResponse)
async def start():
    return base_html_page()


app.include_router(routesbase.routebase)
app.include_router(routesmentor.routementor)
app.include_router(routesadmin.routeadmin)
app.include_router(routesguest.routeguest)


@app.on_event("startup")
async def startup():
    result_check_database = await check_database_exists()
    if not result_check_database: await create_database_if_not_exists()
    await create_shema_for_database()
    await datadb.connect()


@app.on_event("shutdown")
async def shutdown():
    await datadb.disconnect()


if __name__ == "__main__":
    uvicorn.run("main:app", port=8000, host="0.0.0.0", reload=True, workers=4)


