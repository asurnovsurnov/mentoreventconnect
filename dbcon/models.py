from sqlalchemy.orm import relationship, backref
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Table
from datetime import *
from sqlalchemy import inspect
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy_utils import UUIDType
from sqlalchemy.exc import InvalidRequestError
import uuid
from datetime import timedelta
from passlib.hash import bcrypt
from dbcon.database import Base
import sqlalchemy as db



association_guest = db.Table('association_guest', Base.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('guestusers.id', ondelete='CASCADE')),
    db.Column('event_id', db.Integer, db.ForeignKey('events.id', ondelete='CASCADE')),
    db.UniqueConstraint('user_id', 'event_id', name='_user_event_uc'))

class Role(Base, SerializerMixin):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name_role = db.Column(db.String(35), db.CheckConstraint("name_role IN ('first', 'second')"), nullable=False, default="first")

class AdminUser(Base, SerializerMixin):
    __tablename__ = 'adminusers'
    id = db.Column(UUIDType(binary=False), primary_key=True)
    first_name = db.Column(db.String(250), nullable=False)
    last_name = db.Column(db.String(250), nullable=False)
    first_name_telegram = db.Column(db.String(250))
    last_name_telegram = db.Column(db.String(250))
    id_telegram = db.Column(db.Integer)
    phone = db.Column(db.String(250), nullable=False, unique=True)
    email = db.Column(db.String(250), nullable=False, unique=True)
    password = db.Column(db.String(150), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey("roles.id"))
    role = relationship('Role', backref=backref('adminuser', uselist=False))

class MentorUser(Base, SerializerMixin):
    __tablename__ = 'mentorusers'
    id = db.Column(UUIDType(binary=False), primary_key=True)
    first_name = db.Column(db.String(250), nullable=False)
    last_name = db.Column(db.String(250), nullable=False)
    first_name_telegram = db.Column(db.String(250))
    last_name_telegram = db.Column(db.String(250))
    id_telegram = db.Column(db.Integer)
    phone = db.Column(db.String(250), nullable=False, unique=True)
    email = db.Column(db.String(250), nullable=False, unique=True)
    password = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(3800))
    is_blocked = db.Column(db.Boolean(), default=False)
    event = relationship('Event', backref='mentoruser', lazy='joined')

class GuestUser(Base, SerializerMixin):
    __tablename__ = 'guestusers'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(250))
    last_name = db.Column(db.String(250))
    id_telegram = db.Column(db.Integer)
    phone = db.Column(db.String(250), nullable=False, unique=True)
    email = db.Column(db.String(250), unique=True)
    is_blocked = db.Column(db.Boolean(), default=False)
    event = relationship('Event', secondary=association_guest, back_populates='user_guest', lazy='joined')

class Event(Base, SerializerMixin):
    __tablename__ = 'events'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    state = db.Column(db.String(35), nullable=False)
    city = db.Column(db.String(250), nullable=False)
    address = db.Column(db.String(250), nullable=False)
    price = db.Column(db.String(250), nullable=False)
    type = db.Column(db.String(250), nullable=False)
    max_count_guests = db.Column(db.Integer)
    description = db.Column(db.String(3800))
    is_cancel = db.Column(db.Boolean(), default=False)
    mentor_id = db.Column(UUIDType(binary=False), db.ForeignKey('mentorusers.id', ondelete='CASCADE'), nullable=False)
    user_guest = relationship('GuestUser', secondary=association_guest, back_populates='event', lazy='joined')
    review_from_guest = relationship('Review', backref='events', lazy='joined')

class Review(Base, SerializerMixin):
    __tablename__ = 'reviews'
    id = db.Column(db.Integer, primary_key=True)
    from_guest = db.Column(db.Integer, db.ForeignKey('guestusers.id', ondelete='CASCADE'), nullable=False)
    comment = db.Column(db.String(3880), nullable=False)
    rating = db.Column(db.String(150), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    event_id = db.Column(db.Integer, db.ForeignKey('events.id', ondelete='CASCADE'), nullable=False)

class ComplaintGuest(Base, SerializerMixin):
    __tablename__ = 'complaintguests'
    id = db.Column(db.Integer, primary_key=True)
    from_guest = db.Column(db.Integer, db.ForeignKey('guestusers.id', ondelete='CASCADE'), nullable=False)
    comment = db.Column(db.String(3880), nullable=False)
    topic = db.Column(db.String(150), db.CheckConstraint("topic IN ('help', 'error', 'question')"), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    status = db.Column(db.String(100), nullable=False)

class ComplaintMentor(Base, SerializerMixin):
    __tablename__ = 'complaintmentors'
    id = db.Column(db.Integer, primary_key=True)
    from_mentor = db.Column(UUIDType(binary=False), db.ForeignKey('mentorusers.id', ondelete='CASCADE'), nullable=False)
    comment = db.Column(db.String(3880), nullable=False)
    topic = db.Column(db.String(150), db.CheckConstraint("topic IN ('help', 'error', 'question')"), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    status = db.Column(db.String(100), nullable=False)

