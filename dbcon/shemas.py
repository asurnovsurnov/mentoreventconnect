import re
from datetime import datetime, date
from typing import Optional, List, Union
from pydantic import BaseModel, EmailStr, Field, validator
from enum import Enum

class ReviewRating(str, Enum):
    positive = 'positive'
    neutral = 'neutral'
    negative = 'negative'

class Topic(str, Enum):
    help = 'help'
    error= 'error'
    question = 'question'

class Who(str, Enum):
    mentor = 'mentor'
    guest= 'guest'

class TypeEvent(str, Enum):
    lecture = 'Lecture'
    master_class = 'Master Сlass'
    music_show = 'Music Show'
    comedy_show = 'Comedy Show'
    sport_show = 'Sport Show'
    presentation = 'Presentation'
    seminar = 'Seminar'
    conference = 'Conference'
    excursion = 'Excursion'
    tour = 'Tour'
    debate = 'Debate'


class StatusCompliant(str, Enum):
    new = 'new'
    inprogress = 'inprogress'
    done = 'done'
    rejected = 'rejected'

class BaseEvent(BaseModel):
    id: Optional[int] = None
    name: str
    date: datetime
    state: str
    city: str
    address: str
    price: str
    type: Optional[str] = None
    max_count_guests: int
    description: Optional[str] = None
    is_cancel: Optional[bool] = False
    mentor_id: Optional[str] = None


    @validator("date", pre=True)
    def parse_date(cls, value):
        return datetime.strptime(value, '%Y-%m-%d %H:%M:%S')

    class Config:
        orm_mode = True


class BaseMentor(BaseModel):
    id: Optional[str] = None
    first_name: str
    last_name: str
    first_name_telegram: Optional[str] = None
    last_name_telegram: Optional[str] = None
    id_telegram: Optional[int] = None
    phone: str
    email: str
    password: str
    description: Optional[str] = None
    is_blocked: Optional[bool] = False
    event: Optional[List[BaseEvent]] = None

    @validator("email")
    @classmethod
    def validate_email(cls, value):
        if not bool(re.fullmatch(r'[\w.-]+@[\w-]+\.[\w.]+', value)):
            raise ValueError("Email is invalid")
        return value

    @validator('password')
    @classmethod
    def password_validator(cls, value):
        if len(value) <= 8 or len(value) > 21:
            raise ValueError("Password must be between 8 and 20 characters")
        if not re.search(r'\d', value) or not re.search(r'[a-zA-Z]', value):
            raise ValueError("Password must contain at least one digit and one letter")
        return value

    @validator("phone")
    @classmethod
    def validate_phone(cls, value):
        if not bool(re.fullmatch(r'^[\+0-9 ]{6,18}$', value)):
            raise ValueError("Phone number is invalid")
        return value

class BaseAdmin(BaseModel):
    id: Optional[str] = None
    first_name: str
    last_name: str
    first_name_telegram: Optional[str] = None
    last_name_telegram: Optional[str] = None
    id_telegram: Optional[int] = None
    phone: str
    email: str
    password: str
    role_id: Optional[int] = None
    role: Optional[str] = None

    @validator("email")
    @classmethod
    def validate_email(cls, value):
        if not bool(re.fullmatch(r'[\w.-]+@[\w-]+\.[\w.]+', value)):
            raise ValueError("Email is invalid")
        return value

    @validator('password')
    @classmethod
    def password_validator(cls, value):
        if len(value) <= 8 or len(value) > 21:
            raise ValueError("Password must be between 8 and 20 characters")
        if not re.search(r'\d', value) or not re.search(r'[a-zA-Z]', value):
            raise ValueError("Password must contain at least one digit and one letter")
        return value

    @validator("phone")
    @classmethod
    def validate_phone(cls, value):
        if not bool(re.fullmatch(r'^[\+0-9 ]{6,18}$', value)):
            raise ValueError("Phone number is invalid")
        return value

    class Config:
        orm_mode = True

class AdminAuth(BaseAdmin):
    id: Optional[str] = None
    first_name: Optional[str] = Field(default=None, exclude=True)
    last_name: Optional[str] = Field(default=None, exclude=True)
    id_telegram: Optional[int] = Field(default=None, exclude=True)
    phone: Optional[str] = Field(default=None, exclude=True)
    email: str
    password: str

class AdminUpdatePassword(BaseAdmin):
    id: Optional[str] = None
    first_name: Optional[str] = Field(default=None, exclude=True)
    last_name: Optional[str] = Field(default=None, exclude=True)
    id_telegram: Optional[int] = Field(default=None, exclude=True)
    phone: Optional[str] = Field(default=None, exclude=True)
    email: str
    password: str
    new_password: str

    @validator('new_password')
    @classmethod
    def password_validator(cls, value):
        if len(value) <= 8 or len(value) > 21:
            raise ValueError("Password must be between 8 and 20 characters")
        if not re.search(r'\d', value) or not re.search(r'[a-zA-Z]', value):
            raise ValueError("Password must contain at least one digit and one letter")
        return value


class MentorAuth(BaseMentor):
    id: Optional[str] = None
    first_name: Optional[str] = Field(default=None, exclude=True)
    last_name: Optional[str] = Field(default=None, exclude=True)
    id_telegram: Optional[int] = Field(default=None, exclude=True)
    phone: Optional[str] = Field(default=None, exclude=True)
    email: str
    password: str

class AdminChange(AdminAuth):
    email: str
    password: Optional[str] = Field(default=None, exclude=True)

class Review(BaseModel):
    email: str
    comment: str
    date: Optional[datetime] = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")

    @validator("email")
    @classmethod
    def validate_email(cls, value):
        if not bool(re.fullmatch(r"[\w.-]+@[\w-]+\.[\w.]+", value)):
            raise ValueError("Email is invalid")
        return value

    @validator('comment')
    @classmethod
    def comment_validator(cls, value):
        if len(value) <= 8 or len(value) > 3800:
            raise ValueError("Comment must be between 8 and 3800 characters")
        return value

    class Config:
        orm_mode = True

class Complaint(BaseModel):
    email: str
    comment: str
    phone: str
    date: Optional[datetime] = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")

    @validator("email")
    @classmethod
    def validate_email(cls, value):
        if not bool(re.fullmatch(r"[\w.-]+@[\w-]+\.[\w.]+", value)):
            raise ValueError("Email is invalid")
        return value

    @validator("comment")
    @classmethod
    def comment_validator(cls, value):
        if len(value) < 8 or len(value) > 3800:
            raise ValueError("Comment must be between 8 and 3800 characters")
        return value

    @validator("phone")
    @classmethod
    def validate_phone(cls, value):
        if not bool(re.fullmatch(r'^[\+0-9 ]{6,18}$', value)):
            raise ValueError("Phone number is invalid")
        return value

    class Config:
        orm_mode = True

class ComplaintGuest(Complaint):
    email: str
    comment: str
    phone: str
    date: Optional[datetime] = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")

class ComplaintMentor(Complaint):
    email: Optional[str] = Field(default=None, exclude=True)
    comment: str
    phone: Optional[str] = Field(default=None, exclude=True)
    date: Optional[datetime] = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")


class BaseGuest(BaseModel):
    id: Optional[int] = None
    first_name: str
    last_name: str
    id_telegram: Optional[int] = None
    phone: str
    email: str
    is_blocked: Optional[bool] = False

    @validator("email")
    @classmethod
    def validate_email(cls, value):
        if not bool(re.fullmatch(r"[\w.-]+@[\w-]+\.[\w.]+", value)):
            raise ValueError("Email is invalid")
        return value

    @validator("phone")
    @classmethod
    def validate_phone(cls, value):
        if not bool(re.fullmatch(r"^[\+0-9 ]{6,18}$", value)):
            raise ValueError("Phone number is invalid")
        return value

    class Config:
        orm_mode = True

class EventOut(BaseEvent):
    user_guest: Optional[List[BaseGuest]] = []

class GuestOut(BaseGuest):
    event: Optional[List[EventOut]] = []