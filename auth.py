import os
import sys
import time
import jwt                                  # used for encoding and decoding jwt tokens
from fastapi import HTTPException           # used to handle error handling
from passlib.context import CryptContext    # used for hashing the password
from datetime import datetime, timedelta    # used to handle expiry time for tokens
from config import APP_SECRET_STRING, APP_ADMIN_SECRET_STRING, ALGORITM_ONE, ALGORITM_TWO

class Auth():
    hasher = CryptContext(schemes=['bcrypt'])
    secret = APP_SECRET_STRING
    algoritm = ALGORITM_ONE

    def encode_password(self, password):
        return self.hasher.hash(password)

    def verify_password(self, password, encoded_password):
        return self.hasher.verify(password, encoded_password)

    def encode_token(self, username):
        payload = {
            'exp': datetime.utcnow() + timedelta(days=0, minutes=30),
            'iat': datetime.utcnow(),
            'scope': 'access_token',
            'sub': username
        }
        return jwt.encode(
            payload,
            self.secret,
            algorithm=self.algoritm
        )

    def decode_token(self, token):
        try:
            payload = jwt.decode(token, self.secret, algorithms=[self.algoritm])
            if (payload['scope'] == 'access_token'):
                return payload['sub']
            raise HTTPException(status_code=401, detail='Scope for the token is invalid')
        except jwt.ExpiredSignatureError:
            raise HTTPException(status_code=401, detail='Token expired')
        except jwt.InvalidTokenError:
            raise HTTPException(status_code=401, detail='Invalid token')

    def encode_refresh_token(self, username):
        payload = {
            'exp': datetime.utcnow() + timedelta(days=0, hours=10),
            'iat': datetime.utcnow(),
            'scope': 'refresh_token',
            'sub': username
        }
        return jwt.encode(
            payload,
            self.secret,
            algorithm=self.algoritm
        )

    def refresh_token(self, refresh_token):
        try:
            payload = jwt.decode(refresh_token, self.secret, algorithms=[self.algoritm])
            if (payload['scope'] == 'refresh_token'):
                username = payload['sub']
                new_token = self.encode_token(username)
                return new_token
            raise HTTPException(status_code=401, detail='Invalid scope for token')
        except jwt.ExpiredSignatureError:
            raise HTTPException(status_code=401, detail='Refresh token expired')
        except jwt.InvalidTokenError:
            raise HTTPException(status_code=401, detail='Invalid refresh token')

class Authadmin(Auth):
    secret = APP_ADMIN_SECRET_STRING
    algoritm = ALGORITM_TWO


def input_with_retry(prompt, check_func, signal='email'):
    while True:
        if signal=='pass': value = pwinput.pwinput(prompt='Please enter you password: ', mask='*')
        else: value = input(prompt)
        if check_func(value):
            break
        else:
            if signal=='email': print(f'Sorry, email: {value} is invalid. Try again.\n')
            elif signal=='pass': print(f'Sorry, password must be between 8 and 20 characters.\nTry again.\n')
            elif signal=='phone':print(f'Sorry, email: {value} is invalid. \nTry again.\n')

    return value
#Let's create a basic admin to work with the API

if __name__ == "__main__":
    import uuid, re, pwinput
    first_admin_auth = Authadmin()
    print('Welcome! Let"s create a basic admin to work with the API.\n')
    first_name_admin = input('Please enter you first name: ')
    last_name_admin = input('Please enter you last name: ')
    email_admin = input_with_retry('Please enter you email: ', lambda email: bool(re.fullmatch(r'[\w.-]+@[\w-]+\.[\w.]+', email)), signal='email')
    password_admin = input_with_retry('Please enter you password: ', lambda pw: len(pw) > 8 and len(pw) < 21, signal='pass')
    phone_admin = input_with_retry('Please enter you phone number: ', lambda phone: bool(re.fullmatch(r'^[\+0-9 ]{6,18}$', phone)), signal='phone')

    pass_hash = first_admin_auth.encode_password(password_admin)
    id_admin_for_db = uuid.uuid4().hex

    print('-------------------------------------------')
    print('FIRST STEP \nRun the program with help command "python3 main.py" and then stop application.\nThis is necessary to initialize the database.\n')
    time.sleep(3)

    print(f'If you have done this, then ok, you can continue.\n We must to create first admin.')
    print('-------------------------------------------')
    print(f'SECOND STEP \nOpen Database in terminal and past sql commands: \n'
          f'INSERT INTO roles (name_role) VALUES ($$first$$);\n'
          f'INSERT INTO roles (name_role) VALUES ($$second$$);\n')


    print(f'\nNext past sql command:\n'
          f'INSERT INTO adminusers (id, first_name, last_name, phone, email, password, role_id) VALUES ($${id_admin_for_db}$$, $${first_name_admin}$$, $${last_name_admin}$$, $${phone_admin}$$, $${email_admin}$$,$${pass_hash}$$,$$1$$);')
    print('\n-------------------------------------------\n')
    time.sleep(3)
    question_user = input('Do you make sql command? [yes/no]: ').lower().strip()

    if question_user in ('yes', 'y'):
        print('\n-------------------------------------------\n'
          'OK. If you have done this, then you have created an administrator with first level privileges.'
          '\nRun the application again.\nYou can work.')
    elif question_user in ('no', 'n'):
        print('\n-------------------------------------------\n'
              'Please paste the an sql commands into the terminal of database.\nThis is required to create an administrator and after that run the application again.'
              '\nYou can work.')


