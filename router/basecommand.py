from .__init__ import *


async def get_events(skip: int = 0, limit: int = 100):
    event = models.Event.__table__
    query = event.select().offset(skip).limit(limit)
    result_list =await datadb.fetch_all(query)
    return  [dict(result) for result in result_list]

async def check_visit_guest(guest_list: list, guest_email: str):
    for elem in guest_list:
        if guest_email in elem['email']: return elem
    return False

async def get_event(event_id: int):
    event = models.Event.__table__
    query = event.select().where(event.c.id == event_id).select_from(event.join(models.association_guest, isouter=True).join(models.GuestUser.__table__, isouter=True))
    result = await datadb.fetch_one(query)
    if result is None:
        raise HTTPException(status_code=404, detail="Event not found")
    event_data = dict(result)
    event_data["user_guest"] = await get_guest_users_for_event(event_id)
    return event_data

async def check_event(event_id: int):
    event = models.Event.__table__
    query = event.select().where(event.c.id == event_id)
    result = await datadb.fetch_one(query)
    event_data = dict(result)
    return event_data['is_cancel']

async def get_guest_users_for_event(event_id: int):
    query = models.association_guest.select().\
        where(models.association_guest.c.event_id == event_id).\
        select_from(models.association_guest.join(models.GuestUser.__table__)).\
        column(models.GuestUser.__table__.c.phone).column(models.GuestUser.__table__.c.id).column(models.GuestUser.__table__.c.email)
    results = await datadb.fetch_all(query)
    return [dict(result) for result in results]

async def get_mentors(skip: int = 0, limit: int = 100):
    mentor = models.MentorUser.__table__
    query = mentor.select().offset(skip).limit(limit)
    result_list =await datadb.fetch_all(query)
    answer = [dict(result) for result in result_list]
    for elem in answer:
        if elem.get('password'): del elem['password']
    return answer

async def get_mentor(mentor_id: str):
    mentor = models.MentorUser.__table__
    event = models.Event.__table__
    query = mentor.select().select_from(mentor.outerjoin(event, mentor.c.id == event.c.mentor_id)).where(mentor.c.id == mentor_id).column(event.c.id).column(event.c.name).column(event.c.date)
    result = await datadb.fetch_all(query)
    result = [dict(el) for el in result]
    if not result: raise HTTPException(status_code=404, detail=f'Dont have mentor with id: {mentor_id}')
    answer = {}
    for item in result:
        events_list = answer.setdefault('events', [])
        event_dict = {}
        for key in ['id_1', 'name', 'date']:
            event_dict[key] = item.pop(key)
        events_list.append(event_dict)
        answer.update(item)
    if len(answer['events']) == 1 and answer['events'][0]['id_1'] == None: answer['events'] = []
    del answer['password']
    return answer

async def add_guest(guest_details: shemas.BaseGuest):
    guest = models.GuestUser.__table__
    stmt = guest.insert().values(**guest_details.dict(exclude_none=True))
    result = await datadb.execute(stmt)
    #print(f'dict_add_guest: {dict(result)}')
    return result

async def get_guest_in_db(guest_details: str):
    guest = models.GuestUser.__table__
    query = guest.select().where(guest.c.email==guest_details)
    info_of_guest = await datadb.fetch_one(query)
    if info_of_guest: return dict(info_of_guest)
    return False

async def get_guests_list_in_db(skip: int = 0, limit: int = 100):
    guest = models.GuestUser.__table__
    query = guest.select().offset(skip).limit(limit)
    info_of_guests = await datadb.fetch_all(query)
    return [dict(info_guest) for info_guest in info_of_guests]

async def get_reviews(event_id: int, skip: int = 0, limit: int = 100):
    review = models.Review.__table__
    event = models.Event.__table__
    query = event.select().offset(skip).limit(limit).select_from(event.outerjoin(review, event.c.id == review.c.event_id)).where(
        event.c.id == event_id).column(review.c.id).column(review.c.comment).column(review.c.date).column(review.c.from_guest).column(review.c.rating)
    result = await datadb.fetch_all(query)
    result = [dict(el) for el in result]
    answer = {}
    for item in result:
        events_list = answer.setdefault('review', [])
        event_dict = {}
        for key in ['id_1', 'comment', 'date_1', 'from_guest', 'rating']:
            event_dict[key] = item.pop(key)
        events_list.append(event_dict)
        answer.update(item)
    if len(answer['review']) == 1 and answer['review'][0]['id_1'] == None: answer['review'] = []
    return answer