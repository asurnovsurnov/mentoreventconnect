from .__init__ import *
from router import basecommand, guestcommand

routeguest = APIRouter(prefix="/guest", tags=['Guest'])


@routeguest.post('/signup/event')
async def signup_event(event_id: int, guest_details: shemas.BaseGuest):
    """
    Purpose: \n
    Register a guest for an event. \n
    If the guest is not present in the database, a new record will be created.
    """
    info_guest = await basecommand.get_guest_in_db(guest_details=guest_details.email)
    check_event = await basecommand.check_event(event_id=event_id)
    if check_event: raise HTTPException(status_code=404, detail='Sorry, The event is cancel.')
    if not info_guest:
        new_guest = await basecommand.add_guest(guest_details=guest_details)
        info_guest = await basecommand.get_guest_in_db(guest_details=guest_details.email)
    event_guest = await guestcommand.entry_event(event_id=event_id, guest_details_id=info_guest["id"])
    if not event_guest: raise HTTPException(status_code=403, detail='Sorry, you account is blocked!\n Write to the administrator for clarifications for the reason.')
    result = await guestcommand.generate_qr_info(event_id=event_id, guest_details=guest_details)
    img_tick = base64.b64decode(result)
    return StreamingResponse(content=io.BytesIO(img_tick), media_type='image/png')


@routeguest.delete('/cancel/event')
async def delete_visit_on_event(event_id: int, guest_details_id: int):
    """
    Purpose: \n
    Cancel a guest's visit to an event by deleting the corresponding record from the database.
    """
    result = await guestcommand.cancel_visit_on_event(event_id=event_id, guest_details_id=guest_details_id)
    return result


@routeguest.post('/create/review/{event_id}')
async def create_review(event_id: int, rating: ReviewRating, review_details: shemas.Review):
    """
    Purpose: \n
    Create a review from a guest for an event they attended.\n
    If the guest did not attend the event, an HTTPException with status code 403 will be raised.
    """
    guest_list = await basecommand.get_guest_users_for_event(event_id=event_id)
    check_result = await basecommand.check_visit_guest(guest_list=guest_list, guest_email=review_details.email)
    if check_result:
        comment_result = await guestcommand.review_create(guest_id=check_result['id'], event_id=event_id, rating=rating, review_details=review_details)
        return comment_result
    else:
        raise HTTPException(status_code=403, detail='You did not attend this event, therefore you are not allowed to leave a comment!')


@routeguest.put('/update/review')
async def update_review(comment_id: int, rating: ReviewRating, review_details: shemas.Review):
    """
    Purpose: \n
    Update a review from a guest for an event they attended.\n
    If the guest did not attend the event, an HTTPException with status code 403 will be raised.
    """
    info_guest = await basecommand.get_guest_in_db(guest_details=review_details.email)
    if info_guest:
        result = await guestcommand.review_update(guest_id=info_guest['id'],  comment_id=comment_id, rating=rating, review_details=review_details)
        return result
    else:
        raise HTTPException(status_code=403, detail='Sorry, you were not a guest at this event.')


@routeguest.post('/send/complaint')
async def send_complaint(complaint_details: shemas.ComplaintGuest, topic: Topic):
    """
    Purpose: \n
    Send Complaint by Guest. \n
    This method sends a complaint to the technical support administrators on behalf of the Guest User.
    """
    info_guest = await basecommand.get_guest_in_db(guest_details=complaint_details.email)
    if info_guest:
        result = await guestcommand.complaint_send_from_guest(guest_id=info_guest['id'], topic=topic, complaint_details=complaint_details)
        return result
    else:
        raise HTTPException(status_code=403, detail='Sorry, you are not a user of the Internet resource.')


