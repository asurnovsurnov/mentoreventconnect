from .__init__ import *
from router import basecommand, admincommand

routeadmin = APIRouter(prefix="/admin", tags=['Admin'])


@routeadmin.get('/read/guests')
async def read_guests(skip: int = 0, limit: int = 100, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Get Guests List Request.\n
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    return await basecommand.get_guests_list_in_db(skip=skip, limit=limit)

@routeadmin.get('/read/guest/{guest_id}')
async def read_guests(guest_id: int, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Get info guest and him events.\n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    return await admincommand.get_guset_for_admin(guest_id=guest_id)


@routeadmin.get('/read/admins')
async def read_admins(skip: int = 0, limit: int = 100, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Get Adminstrator List Request.\n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    return await admincommand.get_admins(skip=skip, limit=limit)


@routeadmin.get('/read/admin/{admin_email}')
async def get_admin(admin_email: str, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Retrieves administrator's information from the database. \n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    result = await admincommand.get_admin_by_email(email=admin_email)
    if not result: raise HTTPException(status_code=404, detail=f'Dont have admin with email: {admin_email}')
    return result



@routeadmin.get('/complaints')
async def read_complaints(who: shemas.Who, skip: int = 0, limit: int = 100, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Get Сomplaints List Request.\n
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    return await admincommand.get_complaints(who=who, skip=skip, limit=limit)


@routeadmin.get('/complaint/{complaint_id}')
async def read_complaint(complaint_id: int, who: shemas.Who, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Retrieves a complaint's information from the database. \n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised..
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    result = await admincommand.get_complaint(who=who, complaint_id=complaint_id)
    if not result:
        raise HTTPException(status_code=404, detail=f'Dont have complaint from {who} with id: {complaint_id}')
    return result

@routeadmin.get('/get/role')
async def read_role(admin_email: str, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Get the role of an administrator on an internet platform by specifying their email. \n
    Only authorized individuals can access this information.  \n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised..
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    admin_info = await admincommand.get_admin_by_email(email=admin_email)
    if admin_info:
        return await admincommand.check_role_admin(admin_id=admin_info['id'])
    raise HTTPException(status_code=404, detail=f'Dont have admin with email: {admin_email}')


@routeadmin.put('/change/role')
async def change_role(user_details: shemas.AdminChange, role_id: int, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Change the role or privilege of an administrator on an internet platform. Only first-level administrators have the rights \n
    to modify the roles of other administrators. The available roles are divided into two levels: first and second. \n
    If the credentials provided are invalid or if the requestor is not a first-level administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    permission = await admincommand.check_role_admin(admin_id=secret_token)
    if permission['msg'] == 'first':
        admin_change = await admincommand.get_admin_by_email(email=user_details.email)
        if not admin_change: raise HTTPException(status_code=404, detail=f"Not found admin with! {user_details.email}")
        return await admincommand.change_role_admin(admin_id=admin_change['id'], role_id=role_id)
    raise HTTPException(status_code=401, detail="You don't have enough administrator rights!")


@routeadmin.put('/change/password')
async def change_password(user_details: shemas.AdminUpdatePassword, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Change the password of an administrator on an internet platform. \n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    user_details.id = secret_token
    return await admincommand.password_change_admin(user_details=user_details)


@routeadmin.put('/change/complaint/status')
async def change_complaint_status(status: shemas.StatusCompliant, who: Who, complaint_id: int, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Change the status of a user complaint by an administrator on an internet platform. \n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    return await admincommand.complaint_status_change(status=status, who=who, complaint_id=complaint_id)


@routeadmin.post('/register')
async def make_register(user_details: shemas.BaseAdmin, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Register a new administrator on an internet platform. \n
    Only an existing administrator can register another administrator. \n
    The first administrator is created through the database.\n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    return await admincommand.make_singup_admin(user_details=user_details)


@routeadmin.post('/login')
async def login(user_details: shemas.AdminAuth):
    """
    Purpose: \n
    Login Mentor User.\n
    This method authenticates an existing Administrator User in the system by receiving the details in the request body.
    """
    return await admincommand.make_login_admin(user_details=user_details)


@routeadmin.put('/change/blocked')
async def manage_blocked(user_id: Union[str, int], blocked: bool, who_blocked: Who, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Block or unblock a user on an internet platform by an administrator. \n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    return await admincommand.change_blokced(id_user=user_id, who_blocked=who_blocked, blocked=blocked)


@routeadmin.delete('/delete/mentor')
async def delete_mentor(mentor_id: str, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Delete a mentor on an internet platform by an administrator. \n
    If the credentials provided are invalid or if the requestor is not a first-level administrator, an HTTPException with status code 401 is raised..
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    permission = await admincommand.check_role_admin(admin_id=secret_token)
    if permission['msg'] == 'first':
        return await admincommand.mentor_delete(mentor_id=mentor_id)
    raise HTTPException(status_code=401, detail="You don't have enough administrator rights!")


@routeadmin.delete('/delete/guest')
async def delete_guest(guest_id: int, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Delete a guest on an internet platform by an administrator. \n
    If the credentials provided are invalid or if the requestor is not a first-level administrator, an HTTPException with status code 401 is raised..
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    permission = await admincommand.check_role_admin(admin_id=secret_token)
    if permission['msg'] == 'first':
        return await admincommand.guest_delete(guest_id=guest_id)
    raise HTTPException(status_code=401, detail="You don't have enough administrator rights!")


@routeadmin.delete('/delete/review')
async def delete_review(review_id: int, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Delete a review on an internet platform by an administrator. \n
    If the credentials provided are invalid or if the requestor is not an existing administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    result_delete = await admincommand.review_delete(review_id=review_id)
    return result_delete


@routeadmin.delete('/delete/reviews')
async def delete_reviews(event_id: int, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Delete all reviews on an internet platform by an administrator. \n
    If the credentials provided are invalid or if the requestor is not a first-level administrator, an HTTPException with status code 401 is raised.
    """
    secret_token = auth_admin_handler.decode_token(credentials.credentials)
    permission = await admincommand.check_role_admin(admin_id=secret_token)
    if permission['msg'] == 'first':
        return await admincommand.reviews_delete(event_id=event_id)
    raise HTTPException(status_code=401, detail="You don't have enough administrator rights!")

'''
@routeadmin.post('/secret')
async def read_secret_data(user_details: shemas.BaseMentor, credentials: HTTPAuthorizationCredentials = Security(security)):
    token = credentials.credentials
    return await admincommand.get_secret_data_of_admins(token=token)
'''