from .__init__ import *
from router import mentorcommand

routementor = APIRouter(prefix="/mentor", tags=['Mentor'])

#💬
#🧭
@routementor.post('/register')
async def make_register(user_details: shemas.BaseMentor):
    """
    Purpose: \n
    Register Mentor User.\n
    This method registers a new Mentor User in the system by receiving the details in the request body.
    """
    return await mentorcommand.make_singup_mentor(user_details=user_details)


@routementor.post('/login')
async def login(user_details: shemas.MentorAuth):
    """
    Purpose: \n
    Login Mentor User.\n
    This method authenticates an existing Mentor User in the system by receiving the details in the request body.
    """
    return await mentorcommand.make_login_mentor(user_details=user_details)


@routementor.post('/create/event')
async def create_event(info_event: shemas.BaseEvent, type: TypeEvent, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Create Event by Mentor.\n
    This method creates a new Event by the Mentor User by receiving the details in the request body and Event type. \n
    If the credentials provided are invalid an HTTPException with status code 401 is raised.
    """
    name_user = auth_handler.decode_token(credentials.credentials)
    info_event.type = type
    return await mentorcommand.event_create(info_event=info_event, name_user=name_user)


@routementor.put('/cancel/event')
async def event_cancel(event_id: int, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Cancel Event by Mentor.\n
    This method cancels an Event by the Mentor User by taking the event_id as input parameter. \n
    If the credentials provided are invalid an HTTPException with status code 401 is raised or if a mentor do not create the event an HTTPException with status code 404 is raised.
    """
    id_mentor = auth_handler.decode_token(credentials.credentials)
    return await mentorcommand.cancel_event(event_id= event_id, mentor_id=id_mentor)


@routementor.put('/restore/event')
async def event_restore(event_id: int, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose:\n
    Restore Event by Mentor.\n
    This method restores a previously canceled Event by the Mentor User by taking the event_id as input parameter. \n
    If the credentials provided are invalid an HTTPException with status code 401 is raised or if a mentor do not create the event an HTTPException with status code 404 is raised.
    """
    id_mentor = auth_handler.decode_token(credentials.credentials)
    return await mentorcommand.restore_event(event_id= event_id, mentor_id=id_mentor)


@routementor.post('/send/complaint')
async def send_complaint(complaint_details: shemas.ComplaintMentor, topic: Topic, credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Purpose: \n
    Send Complaint by Mentor. \n
    This method sends a complaint to the technical support administrators on behalf of the Mentor User. \n
    If the credentials provided are invalid an HTTPException with status code 401 is raised.
    """
    name_user = auth_handler.decode_token(credentials.credentials)
    return await mentorcommand.complaint_send_from_mentor(mentor_id=name_user, topic=topic,complaint_details=complaint_details)



'''
@routementor.post('/secret')
async def read_secret_data(user_details: shemas.BaseMentor, credentials: HTTPAuthorizationCredentials = Security(security)):
    token = credentials.credentials
    return await mentorcommand.get_secret_data_of_users(token=token)


@routementor.delete('/delete_base')
async def delete_base():
    return await crudcommand.delete_info_from_database()
'''