from .__init__ import *
from router import basecommand

routebase = APIRouter(prefix="/base", tags=['Base'])


@routebase.get('/read/events')
async def read_events(skip: int = 0, limit: int = 100):
    """
    Purpose: \n
    Get Events List Request.\n
    """
    return await basecommand.get_events(skip=skip, limit=limit)


@routebase.get('/read/event/{event_id}')
async def read_event(event_id: int):
    """
    Purpose: \n
    Retrieves event's information from the database. \n
    """
    return await basecommand.get_event(event_id=event_id)


@routebase.get('/read/mentors')
async def read_mentors(skip: int = 0, limit: int = 100):
    """
    Purpose: \n
    Get Mentors List Request.\n
    """
    return await basecommand.get_mentors(skip=skip, limit=limit)


@routebase.get('/read/mentor/{mentor_id}')
async def read_mentor(mentor_id: str):
    """
    Purpose: \n
    Retrieves a mentor's information from the database, including all events they have created. \n
    """
    return await basecommand.get_mentor(mentor_id=mentor_id)


@routebase.get('/read/reviews/{event_id}')
async def read_reviews(event_id: int, skip: int = 0, limit: int = 100):
    """
    Purpose: \n
    Retrieves feedback information for this event from the database. \n
    """
    return await basecommand.get_reviews(event_id=event_id, skip=skip, limit=limit)