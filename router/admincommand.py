from .__init__ import *

async def know_who(who: str):
    if who == 'mentor': return models.ComplaintMentor.__table__
    elif who == 'guest': return models.ComplaintGuest.__table__

async def get_complaints(who: str, skip: int = 0, limit: int = 100):
    complaints = await know_who(who = who)
    query = complaints.select().offset(skip).limit(limit)
    result_list =await datadb.fetch_all(query)
    return  [dict(result) for result in result_list]

async def get_complaint(who: str, complaint_id: int):
    complaints = await know_who(who = who)
    query = complaints.select().where(complaints.c.id==complaint_id)
    try:
        result = dict(await datadb.fetch_one(query))
        return result
    except TypeError:
        return False

async def get_admins(skip: int = 0, limit: int = 100):
    event = models.AdminUser.__table__
    query = event.select().offset(skip).limit(limit)
    result_list =await datadb.fetch_all(query)
    answer = [dict(result) for result in result_list]
    for elem in answer:
        if elem.get('password'): del elem['password']
    return answer


async def get_role_admin(role_id: int):
    role = models.Role.__table__
    query = role.select().where(role.c.id == role_id)
    result = dict(await datadb.fetch_one(query))
    return result

async def get_admin_by_email(email: str):
    user = models.AdminUser.__table__
    query = user.select().where(user.c.email == email)
    result = await datadb.fetch_one(query)
    if result is None: return False
    answer = dict(result)
    del answer['password']
    return answer


async def get_admin_by_id(id: str):
    user = models.AdminUser.__table__
    query = user.select().where(user.c.id == id)
    result = await datadb.fetch_one(query)
    if result is None: return {'msg':f'Dont have admin with id: {id}'}
    return dict(result)

async def check_role_admin(admin_id: str):
    admin = models.AdminUser.__table__
    role = models.Role.__table__
    query = admin.select().select_from(admin.join(role, admin.c.role_id == role.c.id)). \
        where(admin.c.id == admin_id).with_only_columns([admin, role.c.name_role])
    result = dict(await datadb.fetch_one(query))
    return {'msg':f'{result["name_role"]}'}

async def change_role_admin(admin_id: str, role_id: int):
    user = models.AdminUser.__table__
    stmt = user.update().where(user.c.id == admin_id).values(role_id=role_id)
    new_role = await datadb.execute(stmt)
    return {'msg':f'Role of admin made change {role_id}'}

async def make_login_admin(user_details: shemas.AdminAuth):
    user = models.AdminUser.__table__
    query = user.select().where(user.c.email == user_details.email)
    result = dict(await datadb.fetch_one(query))
    if (user is None) or (not auth_admin_handler.verify_password(user_details.password, result['password'])):
        raise HTTPException(status_code=401, detail='Invalid username or Invalid password')
    access_token = auth_admin_handler.encode_token(str(result['id']))
    # refresh_token = auth_handler.encode_refresh_token(user_details.first_name)
    return {'access_token': access_token}

async def password_change_admin(user_details: shemas.AdminUpdatePassword):
    user = models.AdminUser.__table__
    query = user.select().where((user.c.email == user_details.email) & (user.c.id == user_details.id))
    result = dict(await datadb.fetch_one(query))
    if user is None:
        raise HTTPException(status_code=401, detail='Invalid username')
    hashed_password = auth_admin_handler.encode_password(user_details.new_password)
    user_details.new_password = hashed_password
    stmt = user.update().where(user.c.id == user_details.id).values(password=user_details.new_password)
    new_password = await datadb.execute(stmt)
    return {'msg':f'You change password admin: {user_details.email}'}

async def make_singup_admin(user_details: shemas.BaseAdmin):
    check_query = models.AdminUser.__table__.select().where(models.AdminUser.__table__.c.email == user_details.email)
    check_admin = await datadb.fetch_all(check_query)
    #role = models.Role.query.filter(models.Role.name_role == 'second').one()
    if check_admin:
        return {'msg':'Account already exists.'}
    try:
        admin_table = models.AdminUser.__table__
        hashed_password = auth_admin_handler.encode_password(user_details.password)
        user_details.password = hashed_password
        new_admin_id = uuid.uuid4().hex
        stmt = admin_table.insert().values(id=uuid.uuid4().hex, role_id=2, **user_details.dict(exclude_none=True))
        new_user = await datadb.execute(stmt)
        user_info = {'key': user_details.first_name, 'password': hashed_password, 'id': new_admin_id}
        return user_info
    except Exception as e:
        error_msg = 'Failed to signup user'
        return error_msg

async def change_blokced(id_user: Union[str, int], blocked: bool, who_blocked: str):
    if who_blocked == 'mentor': table_blokced, id_user = models.MentorUser.__table__, str(id_user)
    elif who_blocked =='guest': table_blokced, id_user = models.GuestUser.__table__, int(id_user)
    else: return {'msg':'Error, you must set who blokced (mentor / guest).'}
    try:
        stmt = table_blokced.update().where(table_blokced.c.id == id_user).values(is_blocked=blocked)
        result = await datadb.execute(stmt)
        if blocked: return {'msg':f'You make blocked {who_blocked}: {id_user}'}
        return {'msg':f'You make unblocked {who_blocked}: {id_user}'}
    except Exception as e:
        print(f'Errorsql: {e}')
        return f'{e}'

async def guest_delete(guest_id: int):
    guest = models.GuestUser.__table__
    stmt_guest = guest.delete().where(guest.c.id == guest_id)
    result_guest = await datadb.execute(stmt_guest)
    return {'msg':f'You delete guest: {guest_id}'}

async def mentor_delete(mentor_id: str):
    mentor = models.MentorUser.__table__
    stmt= mentor.delete().where(mentor.c.id==mentor_id)
    result = await datadb.execute(stmt)
    return {'msg':f'You delete mentor: {mentor_id}'}

async def review_delete(review_id: int):
    try:
        review = models.Review.__table__
        stmt= review.delete().where(review.c.id==review_id)
        result = await datadb.execute(stmt)
        return {'msg':f'You delete review: {review_id}'}
    except InvalidRequestError as e:
        # handle the error by logging or returning a specific response
        return f'Sorry, table dont have review with id: {review_id}.'

async def reviews_delete(event_id: int):
    try:
        review = models.Review.__table__
        stmt= review.delete().where(review.c.event_id==event_id)
        result = await datadb.execute(stmt)
        return {'msg':f'You delete all reviews of event: {event_id}'}
    except InvalidRequestError as e:
        # handle the error by logging or returning a specific response
        return f'Sorry, error.'

async def complaint_status_change(status: str, who: str, complaint_id: int):
    table_who = await know_who(who=who)
    stmt = table_who.update().where(table_who.c.id == complaint_id).values(status=status)
    result = await datadb.execute(stmt)
    return {'msg':'Status update!'}

async def get_guset_for_admin(guest_id: int):
    guest = models.GuestUser.__table__
    query = guest.select().where(guest.c.id == guest_id).select_from(guest.join(models.association_guest, isouter=True).join(models.Event.__table__, isouter=True))
    result = await datadb.fetch_one(query)
    if result is None:
        raise HTTPException(status_code=404, detail="Guest not found")
    guest_data = dict(result)
    guest_data["event"] = await get_event_for_guest(guest_id)
    return guest_data


async def get_event_for_guest(guest_id: int):
    query = models.association_guest.select().\
        where(models.association_guest.c.user_id == guest_id).\
        select_from(models.association_guest.join(models.Event.__table__)).\
        column(models.Event.__table__.c.name).column(models.Event.__table__.c.city).column(models.Event.__table__.c.date).column(models.Event.__table__.c.state)
    results = await datadb.fetch_all(query)
    return [dict(result) for result in results]


async def delete_info_from_database():
    user = models.MentorUser.__table__
    event = models.Event.__table__
    stmt_user, stmt_event = user.delete(), event.delete()
    del_event = await datadb.execute(stmt_event)
    del_user = await datadb.execute(stmt_user)
    return {'msg':'delete all info'}

'''
async def get_secret_data_of_admins(token: str):
    if (auth_admin_handler.decode_token(token)):
        print(f'dec: {auth_admin_handler.decode_token(token)}')
        return 'Top Secret data only authorized admins can access this info'
'''