from .__init__ import *

#query = user.select().where(user.c.email == user_details.email)
async def make_singup_mentor(user_details: shemas.BaseMentor):
    check_query = models.MentorUser.__table__.select().where(models.MentorUser.__table__.c.email == user_details.email)
    check_mentor = await datadb.fetch_all(check_query)
    if check_mentor:
        return {'msg':'Account already exists'}
    try:
        mentor_table = models.MentorUser.__table__
        hashed_password = auth_handler.encode_password(user_details.password)
        user_details.password = hashed_password
        new_mentor_id = uuid.uuid4().hex
        stmt = mentor_table.insert().values(id=new_mentor_id, **user_details.dict(exclude_none=True))
        new_user = await datadb.execute(stmt)
        user_info = {'key': user_details.first_name, 'password': hashed_password, 'id': new_mentor_id}
        return user_info
    except Exception as e:
        print(e)
        error_msg = 'Failed to signup user'
        return error_msg

async def make_login_mentor(user_details: shemas.MentorAuth):
    user = models.MentorUser.__table__
    query = user.select().where(user.c.email == user_details.email)
    result = dict(await datadb.fetch_one(query))
    if (user is None) or (not auth_handler.verify_password(user_details.password, result['password'])):
        raise HTTPException(status_code=401, detail='Invalid password or Invalid username')
    access_token = auth_handler.encode_token(str(result['id']))
    # refresh_token = auth_handler.encode_refresh_token(user_details.first_name)
    return {'access_token': access_token}

async def event_create(info_event: shemas.BaseEvent, name_user: str):
    user = models.MentorUser.__table__
    event = models.Event.__table__
    query_user = user.select().where(user.c.id == name_user)
    id_user = dict(await datadb.fetch_one(query_user))
    if id_user['is_blocked']: raise HTTPException(status_code=403, detail='Sorry, you account is blocked!\n Write to the administrator for clarifications for the reason.')
    stmt_event = event.insert().values(mentor_id=id_user["id"], **info_event.dict(exclude_none=True))
    result = await datadb.execute(stmt_event)
    return {'msg':'Add new Event.'}

async def cancel_event(event_id: int, mentor_id: str):
    event = models.Event.__table__
    query = event.select().where((event.c.id == event_id) & (event.c.mentor_id == mentor_id))
    check = await datadb.fetch_one(query)
    if check is None:
        raise HTTPException(status_code=404, detail='Sorry, this is not your event.')
    stmt = event.update().where((event.c.id == event_id) & (event.c.mentor_id == mentor_id)).values(is_cancel=True)
    result = await datadb.execute(stmt)
    return {'msg':f'You canceled Event: {event_id}'}

async def restore_event(event_id: int, mentor_id: str):
    event = models.Event.__table__
    query = event.select().where((event.c.id == event_id) & (event.c.mentor_id == mentor_id))
    check = await datadb.fetch_one(query)
    if check is None: raise HTTPException(status_code=404, detail='Sorry, this is not your event.')
    stmt = event.update().where((event.c.id == event_id) & (event.c.mentor_id == mentor_id)).values(is_cancel=False)
    result = await datadb.execute(stmt)
    return {'msg':f'You restore Event: {event_id}'}

async def complaint_send_from_mentor(mentor_id: str, topic: str, complaint_details: shemas.ComplaintMentor):
    try:
        complaint = models.ComplaintMentor.__table__
        stmt = complaint.insert().values(from_mentor=mentor_id, comment=complaint_details.comment, date=complaint_details.date,
                                         topic=topic, status='new')
        result = await datadb.execute(stmt)
        return {'msg': f'We received the above complaint. We will give an answer as soon as possible.'}
    except InvalidRequestError as e:
        # handle the error by logging or returning a specific response
        return f'Sorry, error.'