from fastapi import APIRouter
from fastapi import FastAPI, HTTPException, Path, Security
from fastapi.responses import FileResponse, StreamingResponse
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from sqlalchemy.exc import InvalidRequestError
from dbcon.database import engine, datadb
from dbcon import models, shemas
from dbcon.shemas import *
from qrgeneratorinfo import generatorqr
import io
import base64
import auth
import uuid


security = HTTPBearer()

auth_handler = auth.Auth()

auth_admin_handler = auth.Authadmin()