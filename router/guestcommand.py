from .__init__ import *


async def entry_event(event_id: int, guest_details_id: int):
    event_guest = models.association_guest
    try:
        check_blocked = await check_guest_blocked(guest_id=guest_details_id)
        if check_blocked: return False
        stmt = event_guest.insert().values(user_id=guest_details_id, event_id=event_id)
        result = await datadb.execute(stmt)
        return True
    except Exception as e:
        print(f'Errorsql: {e}')
        return f'Errorsql: {event_id}'

async def generate_qr_info(event_id: int, guest_details: shemas.BaseGuest):
    info_guest_string = f'email: {guest_details.email}\nphone: {guest_details.phone}\n ------------------------------'
    event = models.Event.__table__
    query = event.select().where(event.c.id == event_id)
    info_event = await datadb.fetch_one(query)
    info_event = dict(info_event)
    result_string = f'THE GUEST\n{info_guest_string}\n THE EVENT\nname: {info_event["name"]}\nstate: {info_event["state"]}\ncity: {info_event["city"]}\naddress: {info_event["address"]}\ndate: {info_event["date"]}'
    img_info = generatorqr.get_qrcode(result_string)
    return img_info[0]

async def cancel_visit_on_event(event_id: int, guest_details_id: int):
    event_guest = models.association_guest
    try:
        stmt = event_guest.delete().where((event_guest.c.user_id == guest_details_id) & (event_guest.c.event_id == event_id))
        result_delete = await datadb.execute(stmt)
        return {'msg':f'Guest canceled visit on Event: {event_id}'}
    except Exception as e:
        print(f'Errorsql: {e}')
        return f'{e}'

async def review_create(guest_id: int, event_id: int, rating: str, review_details: shemas.Review):
    new_review = models.Review.__table__
    check_blocked = await check_guest_blocked(guest_id=guest_id)
    if check_blocked: raise HTTPException(status_code=403, detail='Sorry, you account is blocked!\n Write to the administrator for clarifications for the reason.')
    stmt = new_review.insert().values(from_guest=guest_id, event_id=event_id, comment=review_details.comment,
                                      rating=rating, date=review_details.date)
    result = await datadb.execute(stmt)
    return {'msg':'Your comment has been successfully added!'}

async def review_update(guest_id: int, rating: str, comment_id: int, review_details: shemas.Review):
    #try:
    new_review = models.Review.__table__
    query = new_review.select().where((new_review.c.id == comment_id) & (new_review.c.from_guest == guest_id))
    check_comment = await datadb.fetch_one(query)
    check_blocked = await check_guest_blocked(guest_id=guest_id)
    if check_comment is None: raise HTTPException(status_code=404, detail='Sorry, this is not your comment.')
    if check_blocked: raise HTTPException(status_code=403, detail='Sorry, you account is blocked!\n Write to the administrator for clarifications for the reason.')
    stmt = new_review.update().where((new_review.c.id == comment_id) & (new_review.c.from_guest == guest_id)).values(comment=review_details.comment, rating=rating)
    result = await datadb.execute(stmt)
    return {'msg':'You update comment!'}

async def complaint_send_from_guest(guest_id: int, topic: str, complaint_details: shemas.ComplaintGuest):
    try:
        complaint = models.ComplaintGuest.__table__
        stmt = complaint.insert().values(from_guest=guest_id, comment=complaint_details.comment, date=complaint_details.date, topic=topic, status='new')
        result = await datadb.execute(stmt)
        return {'msg':f'We received the above complaint. We will give an answer as soon as possible.'}
    except InvalidRequestError as e:
        # handle the error by logging or returning a specific response
        return f'Sorry, error.'

async def check_guest_blocked(guest_id: int):
    guest = models.GuestUser.__table__
    query = guest.select().where(guest.c.id == guest_id)
    info_of_guest = dict(await datadb.fetch_one(query))
    return info_of_guest['is_blocked']